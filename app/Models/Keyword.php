<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\Models\Response;

class Keyword extends Model
{
    use HasFactory;
    public $timestamps = false;


    public $fillable = [
        "word",
        "response_id"
    ];

    public function product(): BelongsTo
    {
    return $this->belongsTo(Response::class);
    }

    public function response(): BelongsTo

    {
        return $this->belongsTo(Response::class);
    }
}
