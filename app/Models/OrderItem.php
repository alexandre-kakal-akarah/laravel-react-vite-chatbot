<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\Models\Order;
use App\Models\Product;



class OrderItem extends Model
{
    use HasFactory;
    public $timestamps = false;


    public $fillable = [
        "order_id",
        "product_id",
        "quantity",
        "price"
    ];


    public function product(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }

    public function order(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
}
