<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\Models\Response;

class Action extends Model
{
    use HasFactory;
    public $timestamps = false;

    public function responses(): HasMany
    {
        return $this->hasMany(Response::class);
    }
}
