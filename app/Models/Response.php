<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\Models\Keyword;
use App\Models\Action;

class Response extends Model
{
    use HasFactory;
    public $timestamps = false;

    public function keywords(): HasMany
    {
        return $this->hasMany(Keyword::class);
    }

    public function response(): BelongsTo
    {
        return $this->belongsTo(Action::class);
    }

    public function action(): BelongsTo
    {
        return $this->belongsTo(Action::class);
    }
}
