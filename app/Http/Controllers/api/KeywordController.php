<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Keyword;
use Illuminate\Http\Request;

class KeywordController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $keyword = Keyword::all();

        return response()->json($keyword);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $keyword = Keyword::create([
            "word"=>$request->input("word"),
            "response_id"=>$request->input("response_id")
        ]);

        return response()->json($keyword);
    }

    /**
     * Display the specified resource.
     */
    public function show(Keyword $keyword)
    {
        return response()->json($keyword);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Keyword $keyword)
    {
        $keyword->update([
            "word" => $request->input("word")
        ]);

        return response()->json($keyword);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Keyword $keyword)
    {
        $keyword -> delete();

        return response()->json($keyword);
    }
}
