<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Keyword;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $product = Product::all();

        return response()->json($product);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $product = Product::create([
            "name"=>$request->input("name"),
            "description"=>$request->input("description"),
            "category_id"=>$request->input("category_id"),
            "price"=>$request->input("price"),
            "gender"=>$request->input("gender"),
            "size"=>$request->input("size"),
            "color"=>$request->input("color"),
            "rate"=>$request->input("rate")
        ]);

        return response()->json($product);
    }

    /**
     * Display the specified resource.
     */
    public function show(Product $product)
    {
        return response()->json($product);

    }


    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Product $product)
    {
        $product->update([
            "name" => $request->input("name")
        ]);

        return response()->json($product);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Product $product)
    {
        $product -> delete();

        return response()->json($product);
    }
}
