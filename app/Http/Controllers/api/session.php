<?php

use Illuminate\Http\Request;

Route::middleware(['auth:sanctum'])->get('/session', function (Request $request) {
    return $request->session()->all();
});
