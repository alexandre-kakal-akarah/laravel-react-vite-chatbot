<?php

namespace App\Http\Controllers\api;
use App\Http\Controllers\Controller;
use App\Models\OrderItem;
use Illuminate\Http\Request;

class OrderItemController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $orderitem = OrderItem::all();

        return response()->json($orderitem);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $orderitems = OrderItem::create([
            "order_id"=>$request->input("order_id"),
            "product_id"=>$request->input("product_id"),
            "quantity"=>$request->input("quantity"),
            "price"=>$request->input("price"),
        ]);

        return response()->json($orderitems);
    }

    /**
     * Display the specified resource.
     */
    public function show(OrderItem $orderItem)
    {
        return response()->json($orderItem);
        
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, OrderItem $orderItem)
    {
        //Non utilisé
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(OrderItem $orderItem)
    {
        $orderItem -> delete();

        return response()->json($orderItem);
    }
}
