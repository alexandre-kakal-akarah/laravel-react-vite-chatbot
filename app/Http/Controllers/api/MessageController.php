<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\api\CategoryController;
use App\Http\Controllers\api\ProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Models\Response;
use App\Models\Keyword;
use App\Models\Order;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    // random response
    // public function __invoke(Request $request)
    // {
    //     $response = Response::all()->random()->response;
    //     return response()->json([
    //         'response' => $response
    //     ]);
    // }

    // response based on keyword
    public function __invoke(Request $request)
    {
        $isConnected = Auth::check();
        $sentence = $request->input('message');
        // message corresponds au nom de l'input dans le formulaire dans le fichier Form.jsx
        // get response_id from keyword

        // Stock dans un tableau les mots de la phrase
        $words = explode(' ', $sentence);

        // collect() permet de créer une collection, c'est un tableau amélioré qui va me
        // permettre det trouver plus facilement le keyword avec le plus de poids grâce à la fonction max()
        $keywords = collect();

        // create a collection of keywords
        foreach ($words as $word) {
            $keyword = Keyword::where('word', 'like', "$word")->first();
            // $keyword récupère l'objet keyword qui correspond au mot-clé

            if ($keyword) {
                $keywords->push($keyword);
            }
        }


        if (count($keywords) == 0) {
            return response()->json(['response' => "Je n'ai pas compris votre demande, pourriez-vous reformuler ? 🤔"]);
        }

        // among the keywords, get the one with the highest weight
        // the connection and the registration are the most important.
        // However, if the user is already connected or registered, the connection is not important anymore
        $sorted = $keywords->sortBy(function (Keyword $keyword) {
            return $keyword->response->action->weight;
        });

        // last correspond au dernier élément du tableau
        // le dernier élément du tableau est celui qui a le plus de poids
        $keywordObject = $sorted->last();

        $response = $keywordObject->response;
        // response corresponds à la fonction response() dans le fichier Keyword.php
        // sans les parenthèse, on récupère la relation
        // avec les parenthèse, on récupère l'objet
        // dans la relation, on récupère l'objet response qui correspond à la clé étrangère response_id

        // DEBUT ALGO CHATBOT
        $responseAction = $response->action->action;

        // get categories if action is 'categorie' and search for the category
        // or get all categories if action is 'catalogue'
        if ($responseAction == 'categorie' || $responseAction == 'catalogue') {
            // get all categories if action is 'catalogue'
            $categoriesName = DB::table('categories')->pluck('name');
            if ($responseAction === 'catalogue') {
                return response()->json([
                    'response' => $response->response,
                    'data' => $categoriesName,
                    'action' => $responseAction
                ]);
            }
        // category search
            $categoriesFromDB = (new CategoryController)->index();
            $productsFromDB = (new ProductController)->index();

            $content = $categoriesFromDB->getData();
            $outputCategory = '';
            foreach ($words as $word) {
                foreach ($content as $category) {
                    if ($word == $category->name) {
                        $outputCategory = $category->name;
                        // récupérer les produits de la catégorie
                        $outputProducts = [];
                        foreach ($productsFromDB->getData() as $product) {
                            if ($product->category_id == $category->id) {
                                $outputProducts[] = $product;
                            }
                        }
                        return response()->json([
                            'response' => 'Voici les produits de la catégorie ' . $outputCategory . ' :',
                            'action' => $responseAction,
                            'products' => $outputProducts
                        ]);
                    }
                }
            }
            if ($outputCategory == '') {
                return response()->json([
                    'response' => 'Quelle catégorie souhaitez-vous consulter ?',
                    'data' => $categoriesName,
                    'action' => $responseAction
                ]);
            }
        }

        if ($responseAction === 'ajout-panier') {
            if (!$isConnected) {
                return response()->json([
                    'response' => 'Vous devez être connecté(e) pour ajouter un produit à votre panier.',
                ]);
            }
            // on récupère l'utilisateur connecté grâce à son token
            $user = Auth::user();
            $productsFromDB = (new ProductController)->index();
            $content = $productsFromDB->getData();
            $outputProduct = '';
            foreach ($words as $word) {
                foreach ($content as $product) {
                    if ($word == $product->name) {
                        // produit à ajouter au panier
                        $outputProduct = $product;
                        // // créer une nouvelle commande pour l'utilisateur
                        $order = new Order;
                        $order->total = $outputProduct->price; // TO DO : récupérer le prix de la commande
                        $order->user_id = 1; // TO DO : récupérer l'id de l'utilisateur connecté
                        $order->status = 'new';
                        // dd($order);
                        // $order->save(); --> ne fonctionne pas : erreur 500

                        // // récupérer l'id de la commande créée
                        // $orderId = $order->id;

                        // // Insérer une nouvelle ligne dans la table order_items
                        // $orderItem = [
                        //     'order_id' => $orderId,
                        //     'product_id' => $outputProduct->id,
                        //     'quantity' => 1, // TO DO : récupérer la quantité saisie par l'utilisateur
                        //     'price' => $outputProduct->price
                        // ];

                        // DB::table('order_items')->insert($orderItem);


                        // réponse renvoyée au front
                        return response()->json([
                            'response' => 'Le produit "' . $outputProduct->name . '" a bien été ajouté à votre panier.',
                            'action' => $responseAction,
                            'data' => $outputProduct,
                        ]);
                    }
                }
            }
            if ($outputProduct == '') {
                return response()->json([
                    'response' => 'Quel produit souhaitez-vous ajouter à votre panier ?',
                    'action' => $responseAction,
                    'data' => ''
                ]);
            }
        }

        if ($responseAction === 'connexion') {
            if ($isConnected) {
                return response()->json([
                    'response' => 'Nous sommes connectés !',
                ]);
            }
            return response()->json([
                'response' => $response->response,
                'action' => $responseAction,
                'data' => ''
            ]);
        }

        if ($responseAction === 'registration') {
            return response()->json([
                'response' => $response->response,
                'action' => $responseAction,
                'data' => ''
            ]);
        }

        if ($responseAction === 'commander') {
            if (!$isConnected) {
                return response()->json([
                    'response' => 'Vous devez être connecté(e) pour commander',
                ]);
            }
            return response()->json([
                'response' => $response->response,
                'action' => $responseAction
            ]);
        }

        if ($responseAction === 'consulter-panier') {
            if (!$isConnected) {
                return response()->json([
                    'response' => 'Vous devez être connecté(e) pour consulter votre panier',
                ]);
            }

            // TO DO : récupérer l'id de l'utilisateur connecté et ses commandes
            $orders = [];
            // $orders = DB::table('orders')->where('user_id', 1)->get();

            return response()->json([
                'response' => $response->response,
                'action' => $responseAction,
                'data' => $orders
            ]);
        }

        return response()->json(
            $response
        );

    }
}
