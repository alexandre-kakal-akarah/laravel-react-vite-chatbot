import { create } from "zustand";

interface CategoryStore {
    categoryText: string | null;
    setCategoryText: (categoryText: string) => void;
}

export const useCategoryStore = create<CategoryStore>((set) => ({
    categoryText: null,
    setCategoryText: (categoryText) => set({ categoryText }),
}));
