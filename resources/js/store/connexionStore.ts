import { create } from "zustand";

interface ConnexionStore {
    connexionIsOpen: boolean;
    setConnexionIsOpen: (connexionIsOpen: boolean) => void;
}

export const useConnexionStore = create<ConnexionStore>((set) => ({
    connexionIsOpen: false,
    setConnexionIsOpen: (connexionIsOpen) => set({ connexionIsOpen }),
}));
