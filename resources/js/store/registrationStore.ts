import { create } from "zustand";

interface RegistrationStore {
    registrationIsOpen: boolean;
    setRegistrationIsOpen: (registrationIsOpen: boolean) => void;
}

export const useRegistrationStore = create<RegistrationStore>((set) => ({
    registrationIsOpen: false,
    setRegistrationIsOpen: (registrationIsOpen) => set({ registrationIsOpen }),
}));
