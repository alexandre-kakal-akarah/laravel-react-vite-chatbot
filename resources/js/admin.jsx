import "../css/app.css";
import "../style/globals.scss";

import ReactDOM from "react-dom/client";
import React from "react";
import Nav from "./components/Navigation/Nav";
import { BrowserRouter, Outlet, Route, Routes } from "react-router-dom";
import Categories from "./pages/Categories";
import Dashboard from "./Pages/dashboard";
import Keywords from "./pages/Keywords";
import Orders from "./pages/Orders";
import Product from "./pages/Product";
import Login from "./Pages/Login";

ReactDOM.createRoot(document.getElementById("dashboard")).render(
    <React.StrictMode>
        <>
            <div className="flex">
                <BrowserRouter>
                    <Routes>
                        <Route
                            path="/dashboard"
                            element={
                                <div className="flex">
                                    <Nav></Nav>
                                    <Outlet />
                                </div>
                            }
                        >
                            <Route index element={<Dashboard />} />
                            <Route path="keywords" element={<Keywords />} />
                            <Route path="orders" element={<Orders />} />
                            <Route path="products" element={<Product />} />
                            <Route path="categories" element={<Categories />} />
                            <Route path="*" />
                        </Route>
                        <Route path="login" element={<Login />} />
                    </Routes>
                </BrowserRouter>
            </div>
        </>
    </React.StrictMode>
);
