import Form from "../components/DashboardForm/Form";
import React, { useEffect, useState } from "react";
import { getUser } from "../utils/data";

function Keywords() {
    const token = localStorage.getItem("token");
    const [user, setUser] = useState();

    useEffect(() => {
        getUser("GET", token).then((data) => {
            setUser(data);
        });
    }, []);

    if (user) {
        console.log("user", user);
        if (!token || user.role !== "admin") {
            console.log("ok");
            window.location.href = "/login";
        }
    } else {
        console.log("loading");
    }

    return (
        <div className="w-[78vw] gap-5 flex justify-between items-center pl-10 pr-10 pt-9">
            <div className="w-[40%]">
                <Form title="Mots Clés" route="keyword" info="word" />
            </div>
            <div className="w-[60%]">
                <Form
                    title="Message retourné"
                    route="response"
                    info="response"
                />
            </div>
        </div>
    );
}

export default Keywords;
