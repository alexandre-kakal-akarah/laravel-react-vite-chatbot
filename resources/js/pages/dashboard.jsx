import React, { useEffect, useState } from "react";
import Form from "../components/DashboardForm/Form";
import { getUser } from "../utils/data";

function Dashboard() {
    const token = localStorage.getItem("token");
    const [user, setUser] = useState();

    useEffect(() => {
        getUser("GET", token).then((data) => {
            setUser(data);
        });
    }, []);

    if (user) {
        console.log("user", user);
        if (!token || user.role !== "admin") {
            console.log("ok");
            window.location.href = "/login";
        }
    } else {
        console.log("loading");
    }

    return (
        <div className="w-full">
            <div className="text-center text-[#0F6AE6] font-[Oswald] py-2 px-2 mb-5 mt-5 text-4xl">
                Chatbot Sneak Me
            </div>
            <div className="text-center text-[#0F6AE6] font-[Oswald] py-2 px-2 mb-5 mt-3 text-2xl">
                Bonjour #ADMIN, bienvenue sur le tableau de bord de votre
                chatbot !
            </div>
            <div className="gap-4 flex justify-between items-center pl-10 pr-10 sm:flex-col sm:pl-0 sm:pr-0 sm:justify-center">
                <div className="w-[30%]">
                    <Form title="Catégories" />
                </div>
                <div className="w-[30%]">
                    <Form title="Messages" />
                </div>
                <div className="w-[30%]">
                    <Form title="Produits" />
                </div>
                <div className="w-[30%]">
                    <Form title="Commandes" />
                </div>
            </div>
        </div>
    );
}

export default Dashboard;
