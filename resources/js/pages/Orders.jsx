import Form from "../components/DashboardForm/Form";
import React, { useEffect, useState } from "react";
import { getUser } from "../utils/data";

function Orders() {
    const token = localStorage.getItem("token");
    const [user, setUser] = useState();

    useEffect(() => {
        getUser("GET", token).then((data) => {
            setUser(data);
        });
    }, []);

    if (user) {
        console.log("user", user);
        if (!token || user.role !== "admin") {
            console.log("ok");
            window.location.href = "/login";
        }
    } else {
        console.log("loading");
    }

    return (
        <div className="w-[78vw] gap-5 flex justify-between items-center pl-10 pr-10 pt-9">
            <div className="w-[50%]">
                <Form title="Commandes" />
            </div>
            <div className="w-[50%]">
                <Form title="Résumé de la commande" />
            </div>
        </div>
    );
}

export default Orders;
