import React, { useState, useEffect, useRef } from "react";
import Form from "../components/Form/Form";
import Message from "../components/Message/Message";
import styles from "./MessageContainer.module.scss";

const MessageContainer = () => {
    const [message, setMessage] = useState([
        { provider: "server", message: "Bonjour ! Bienvenu sur SneakMe 👋" },
    ]);

    const scrollToEndRef = useRef();

    useEffect(() => {
        scrollToEndRef.current.scrollIntoView({ behavior: "smooth" });
    }, [message]);

    return (
        <>
            <div className={styles.MessageContainer}>
                {message.map((message, index) => {
                    return (
                        <>
                            <Message key={index} message={message} />
                        </>
                    );
                })}
                <div ref={scrollToEndRef} />
            </div>
            <Form setState={setMessage} message={message} />
        </>
    );
};

export default MessageContainer;
