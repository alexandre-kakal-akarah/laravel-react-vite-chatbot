import React, { useState } from "react";
import Header from "../components/Header/Header";
import MessageContainer from "./MessageContainer";
import styles from "./app.module.scss";
import ModalConnexion from "../components/ModalConnexion/ModalConnexion";
import ModalRegistration from "../components/ModalRegistration/ModalRegistration";

export const AppWrapper = () => {
    const [isOpen, setIsOpen] = useState(false);

    return (
        <React.StrictMode>
            <header>
                <link
                    rel="stylesheet"
                    type="text/css"
                    charSet="UTF-8"
                    href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css"
                />
                <link
                    rel="stylesheet"
                    type="text/css"
                    href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css"
                />
            </header>
            <img
                src="/assets/chat-bleu.png"
                className={styles.mobileOpenHandler}
                onClick={() => setIsOpen(true)}
            />
            <div className={[isOpen ? styles.Open : styles.Closed]}>
                <Header isOpen={isOpen} setIsOpen={setIsOpen} />
                <MessageContainer />
                <ModalConnexion />
                <ModalRegistration />
            </div>
        </React.StrictMode>
    );
};
