export const sendApi = async (method, bodyObject, endPoint, token) => {
    let responsedata;
    let requestOptions;

    if (method === "GET") {
        requestOptions = {
            method: method,
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
            },
        };
    } else {
        requestOptions = {
            method: method,
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
            },
            body: JSON.stringify(bodyObject),
        };
    }

    await fetch(`http://localhost:8000/api/` + `${endPoint}`, requestOptions)
        .then((response) => response.json())
        .then((data) => {
            responsedata = data;
        });

    return responsedata;
};



export const getUser = async (method, token) => {
    console.log('getUser')
    let requestOptions = {
        method: method,
        headers: { 
            "Content-Type": "application/json",
            "Authorization": `Bearer ${token}`
        },
        
    };
    let response = await fetch(`http://localhost:8000/api/me`, requestOptions);
    response = await response.json();

    return response;
}