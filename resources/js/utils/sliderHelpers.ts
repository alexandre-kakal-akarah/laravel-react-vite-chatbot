/**
 *
 * @param array Tableau des pointures
 * @param num Pointure à comparer
 * @returns Pointure la plus proche
 * @description Retourne la pointure la plus proche de celle passée en paramètre pour snapper le slider sur cette pointure
 */
export const arrayClosest = (array: Array<number>, num: number) => {
    let curr = array[0];
    let diff = Math.abs(num - curr);
    for (let val = 0; val < array.length; val++) {
        let newdiff = Math.abs(num - array[val]);
        if (newdiff < diff) {
            diff = newdiff;
            curr = array[val];
        }
    }
    return curr;
};
