import React, { useEffect } from "react";
import styles from "./Message.module.scss";
import Caroussel from "../Caroussel/Caroussel";
import Tags from "../Tag/Tags";
import { useRegistrationStore } from "../../store/registrationStore";
import { useConnexionStore } from "../../store/connexionStore";

const Message = ({ message }) => {
    const { setRegistrationIsOpen } = useRegistrationStore();
    const { setConnexionIsOpen } = useConnexionStore();

    useEffect(() => {
        if (message.action == "connexion") return setConnexionIsOpen(true);
        if (message.action == "registration")
            return setRegistrationIsOpen(true);
    }, [message.action]);

    if (message.message == null || message.message == "") return null;

    const renderActionComponent = (action) => {
        // TO DO: add other actions components (FRONTEND)
        if (action == null || action === undefined) return null;
        switch (action) {
            case "categorie":
                return <Caroussel message={message} />;
            case "commander":
                return null;
            case "ajout-panier":
                return null;
            case "consulter-panier":
                return null;
            case "catalogue":
                return <Tags tagName={message.data.data} />;
            default:
                return null;
        }
    };

    return (
        <>
            <div
                className={
                    message.provider == "client" ? styles.client : styles.server
                }
            >
                {message.message}
            </div>
            {renderActionComponent(message.action)}
        </>
    );
};

export default Message;
