import React, { useEffect, useState } from "react";
import styles from "./ModalConnexion.module.scss";
import { useConnexionStore } from "../../store/connexionStore";
import { sendApi } from "../../utils/data";

const ModalConnexion = () => {
    const { connexionIsOpen, setConnexionIsOpen } = useConnexionStore();
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState(null);

    useEffect(() => {
        console.log("connexionIsOpen", connexionIsOpen);
    }, [connexionIsOpen]);

    const closeHandler = () => {
        console.log("clicked");
        setConnexionIsOpen(false);
        setError(null);
    };

    const login = async () => {
        console.log("trying to login");
        setError(null);
        sendApi("POST", { email: email, password: password }, "login", "").then(
            (response) => {
                console.log(response);
                if (response.status === 200) {
                    localStorage.setItem("token", response.access_token);
                    return setConnexionIsOpen(false);
                }
                if (response.status === 401) {
                    return setError("Email ou mot de passe incorrect");
                }
            }
        );
    };

    return (
        <>
            {connexionIsOpen && (
                <form className={styles.divCo}>
                    <input
                        type="text"
                        name="email"
                        id="connexion-email"
                        className={styles.Email}
                        placeholder="Votre e-mail"
                        onChange={(e) => setEmail(e.target.value)}
                    />
                    <input
                        type="password"
                        id="connexion-password"
                        className={styles.Email}
                        placeholder="Mot de passe"
                        name="password"
                        onChange={(e) => setPassword(e.target.value)}
                    />
                    {error && <div className={styles.error}>{error}</div>}
                    <input
                        type="button"
                        id="connexion-button"
                        value="Connexion"
                        className={styles.Connexion}
                        onClick={(e) => {
                            e.preventDefault();
                            login();
                        }}
                    />
                    <div
                        type="button"
                        className={styles.Connexion}
                        onClick={(e) => {
                            e.preventDefault();
                            closeHandler();
                        }}
                    >
                        Annuler
                    </div>
                </form>
            )}
        </>
    );
};

export default ModalConnexion;
