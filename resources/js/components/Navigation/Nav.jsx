import React from "react";
import Logout from "../Logout";
import { useLocation, NavLink } from "react-router-dom";
import { Fragment, useState } from "react";
import { Dialog, Transition } from "@headlessui/react";
import {
    Bars3Icon,
    ClipboardIcon,
    ShoppingCartIcon,
    HomeIcon,
    TruckIcon,
    ChatBubbleLeftRightIcon,
    XMarkIcon,
} from "@heroicons/react/24/outline";

function classNames(...classes) {
    return classes.filter(Boolean).join(" ");
}

export default function Nav() {
    const [sidebarOpen, setSidebarOpen] = useState(false);

    const location = useLocation();

    return (
        <>
            <div>
                <Transition.Root show={sidebarOpen} as={Fragment}>
                    <Dialog
                        as="div"
                        className="relative z-50 lg:hidden"
                        onClose={setSidebarOpen}
                    >
                        <Transition.Child
                            as={Fragment}
                            enter="transition-opacity ease-linear duration-300"
                            enterFrom="opacity-0"
                            enterTo="opacity-100"
                            leave="transition-opacity ease-linear duration-300"
                            leaveFrom="opacity-100"
                            leaveTo="opacity-0"
                        >
                            <div className="fixed inset-0 bg-gray-900/80" />
                        </Transition.Child>

                        <div className="fixed inset-0 flex">
                            <Transition.Child
                                as={Fragment}
                                enter="transition ease-in-out duration-300 transform"
                                enterFrom="-translate-x-full"
                                enterTo="translate-x-0"
                                leave="transition ease-in-out duration-300 transform"
                                leaveFrom="translate-x-0"
                                leaveTo="-translate-x-full"
                            >
                                <Dialog.Panel className="relative mr-16 flex w-full max-w-xs flex-1">
                                    <Transition.Child
                                        as={Fragment}
                                        enter="ease-in-out duration-300"
                                        enterFrom="opacity-0"
                                        enterTo="opacity-100"
                                        leave="ease-in-out duration-300"
                                        leaveFrom="opacity-100"
                                        leaveTo="opacity-0"
                                    >
                                        <div className="absolute left-full top-0 flex w-16 justify-center pt-5">
                                            <button
                                                type="button"
                                                className="-m-2.5 p-2.5"
                                                onClick={() =>
                                                    setSidebarOpen(false)
                                                }
                                            >
                                                <span className="sr-only">
                                                    Fermer le menu
                                                </span>
                                                <XMarkIcon
                                                    className="h-6 w-6 text-white"
                                                    aria-hidden="true"
                                                />
                                            </button>
                                        </div>
                                    </Transition.Child>
                                    <div className="flex grow flex-col gap-y-5 overflow-y-auto bg-[#2765EC] px-6 pb-4">
                                        <div className="grid justify-center justify-items-center h-16 items-center">
                                            <img
                                                className="flex h-14 w-auto mt-4"
                                                src="https://cdn.discordapp.com/attachments/998637344724885555/1111566496536211516/Avatar_Cube_Groupes.png"
                                                alt="Logo Sneak Me"
                                            />
                                        </div>
                                        <nav className="flex flex-1 flex-col">
                                            <ul
                                                role="list"
                                                className="flex flex-1 flex-col gap-y-7"
                                            >
                                                <li className="mx-2 space-y-1">
                                                    <NavLink
                                                        to="/dashboard"
                                                        className={
                                                            location.pathname ===
                                                            "/dashboard"
                                                                ? "group -mx-2 flex gap-x-3 rounded-md p-2 text-sm font-semibold leading-6 text-[#2765EC] bg-white"
                                                                : "group -mx-2 flex gap-x-3 rounded-md p-2 text-sm font-semibold leading-6 text-indigo-200 hover:bg-[#FFFFFF] hover:text-[#2765EC]"
                                                        }
                                                    >
                                                        <HomeIcon
                                                            className="h-6 w-6 shrink-0 text-indigo-200 group-hover:text-[#2765EC]"
                                                            aria-hidden="true"
                                                        />
                                                        Accueil
                                                    </NavLink>
                                                    <NavLink
                                                        to="/dashboard/keywords"
                                                        className={
                                                            location.pathname ===
                                                            "/dashboard/keywords"
                                                                ? "group -mx-2 flex gap-x-3 rounded-md p-2 text-sm font-semibold leading-6 text-[#2765EC] bg-white"
                                                                : "group -mx-2 flex gap-x-3 rounded-md p-2 text-sm font-semibold leading-6 text-indigo-200 hover:bg-[#FFFFFF] hover:text-[#2765EC]"
                                                        }
                                                    >
                                                        <ChatBubbleLeftRightIcon
                                                            className="h-6 w-6 shrink-0 text-indigo-200 group-hover:text-[#2765EC]"
                                                            aria-hidden="true"
                                                        />
                                                        Mots-clés
                                                    </NavLink>
                                                    <NavLink
                                                        to="/dashboard/orders"
                                                        className={
                                                            location.pathname ===
                                                            "/dashboard/orders"
                                                                ? "group -mx-2 flex gap-x-3 rounded-md p-2 text-sm font-semibold leading-6 text-[#2765EC] bg-white"
                                                                : "group -mx-2 flex gap-x-3 rounded-md p-2 text-sm font-semibold leading-6 text-indigo-200 hover:bg-[#FFFFFF] hover:text-[#2765EC]"
                                                        }
                                                    >
                                                        <TruckIcon
                                                            className="h-6 w-6 shrink-0 text-indigo-200 group-hover:text-[#2765EC]"
                                                            aria-hidden="true"
                                                        />
                                                        Commandes
                                                    </NavLink>
                                                    <NavLink
                                                        to="/dashboard/products"
                                                        className={
                                                            location.pathname ===
                                                            "/dashboard/products"
                                                                ? "group -mx-2 flex gap-x-3 rounded-md p-2 text-sm font-semibold leading-6 text-[#2765EC] bg-white"
                                                                : "group -mx-2 flex gap-x-3 rounded-md p-2 text-sm font-semibold leading-6 text-indigo-200 hover:bg-[#FFFFFF] hover:text-[#2765EC]"
                                                        }
                                                    >
                                                        <ShoppingCartIcon
                                                            className="h-6 w-6 shrink-0 text-indigo-200 group-hover:text-[#2765EC]"
                                                            aria-hidden="true"
                                                        />
                                                        Produits
                                                    </NavLink>
                                                    <NavLink
                                                        to="/dashboard/categories"
                                                        className={
                                                            location.pathname ===
                                                            "/dashboard/categories"
                                                                ? "group -mx-2 flex gap-x-3 rounded-md p-2 text-sm font-semibold leading-6 text-[#2765EC] bg-white"
                                                                : "group -mx-2 flex gap-x-3 rounded-md p-2 text-sm font-semibold leading-6 text-indigo-200 hover:bg-[#FFFFFF] hover:text-[#2765EC]"
                                                        }
                                                    >
                                                        <ClipboardIcon
                                                            className="h-6 w-6 shrink-0 text-indigo-200 group-hover:text-[#2765EC]"
                                                            aria-hidden="true"
                                                        />
                                                        Catégories
                                                    </NavLink>
                                                    <Logout />
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                </Dialog.Panel>
                            </Transition.Child>
                        </div>
                    </Dialog>
                </Transition.Root>

                <div className="hidden lg:fixed lg:inset-y-0 lg:z-50 lg:flex lg:w-72 lg:flex-col">
                    <div className="flex grow flex-col gap-y-5 overflow-y-auto bg-[#2765EC] px-6 pb-4">
                        <div className="grid justify-center justify-items-center h-16 items-center">
                            <img
                                className="flex h-14 w-auto mt-4"
                                src="https://cdn.discordapp.com/attachments/998637344724885555/1111566496536211516/Avatar_Cube_Groupes.png"
                                alt="Logo Sneak Me"
                            />
                        </div>
                        <nav className="flex flex-1 flex-col">
                            <ul
                                role="list"
                                className="flex flex-1 flex-col gap-y-7"
                            >
                                <li className="mx-2 space-y-1">
                                    <NavLink
                                        to="/dashboard"
                                        className={
                                            location.pathname === "/dashboard"
                                                ? "group -mx-2 flex gap-x-3 rounded-md p-2 text-sm font-semibold leading-6 text-[#2765EC] bg-white"
                                                : "group -mx-2 flex gap-x-3 rounded-md p-2 text-sm font-semibold leading-6 text-indigo-200 hover:bg-[#FFFFFF] hover:text-[#2765EC]"
                                        }
                                    >
                                        <HomeIcon
                                            className="h-6 w-6 shrink-0 text-indigo-200 group-hover:text-[#2765EC]"
                                            aria-hidden="true"
                                        />
                                        Accueil
                                    </NavLink>
                                    <NavLink
                                        to="/dashboard/keywords"
                                        className={
                                            location.pathname ===
                                            "/dashboard/keywords"
                                                ? "group -mx-2 flex gap-x-3 rounded-md p-2 text-sm font-semibold leading-6 text-[#2765EC] bg-white"
                                                : "group -mx-2 flex gap-x-3 rounded-md p-2 text-sm font-semibold leading-6 text-indigo-200 hover:bg-[#FFFFFF] hover:text-[#2765EC]"
                                        }
                                    >
                                        <ChatBubbleLeftRightIcon
                                            className="h-6 w-6 shrink-0 text-indigo-200 group-hover:text-[#2765EC]"
                                            aria-hidden="true"
                                        />
                                        Mots-clés
                                    </NavLink>
                                    <NavLink
                                        to="/dashboard/orders"
                                        className={
                                            location.pathname ===
                                            "/dashboard/orders"
                                                ? "group -mx-2 flex gap-x-3 rounded-md p-2 text-sm font-semibold leading-6 text-[#2765EC] bg-white"
                                                : "group -mx-2 flex gap-x-3 rounded-md p-2 text-sm font-semibold leading-6 text-indigo-200 hover:bg-[#FFFFFF] hover:text-[#2765EC]"
                                        }
                                    >
                                        <TruckIcon
                                            className="h-6 w-6 shrink-0 text-indigo-200 group-hover:text-[#2765EC]"
                                            aria-hidden="true"
                                        />
                                        Commandes
                                    </NavLink>
                                    <NavLink
                                        to="/dashboard/products"
                                        className={
                                            location.pathname ===
                                            "/dashboard/products"
                                                ? "group -mx-2 flex gap-x-3 rounded-md p-2 text-sm font-semibold leading-6 text-[#2765EC] bg-white"
                                                : "group -mx-2 flex gap-x-3 rounded-md p-2 text-sm font-semibold leading-6 text-indigo-200 hover:bg-[#FFFFFF] hover:text-[#2765EC]"
                                        }
                                    >
                                        <ShoppingCartIcon
                                            className="h-6 w-6 shrink-0 text-indigo-200 group-hover:text-[#2765EC]"
                                            aria-hidden="true"
                                        />
                                        Produits
                                    </NavLink>
                                    <NavLink
                                        to="/dashboard/categories"
                                        className={
                                            location.pathname ===
                                            "/dashboard/categories"
                                                ? "group -mx-2 flex gap-x-3 rounded-md p-2 text-sm font-semibold leading-6 text-[#2765EC] bg-white"
                                                : "group -mx-2 flex gap-x-3 rounded-md p-2 text-sm font-semibold leading-6 text-indigo-200 hover:bg-[#FFFFFF] hover:text-[#2765EC]"
                                        }
                                    >
                                        <ClipboardIcon
                                            className="h-6 w-6 shrink-0 text-indigo-200 group-hover:text-[#2765EC]"
                                            aria-hidden="true"
                                        />
                                        Catégories
                                    </NavLink>
                                </li>
                                <Logout />
                            </ul>
                        </nav>
                    </div>
                </div>

                <div className="lg:pl-72">
                    <button
                        type="button"
                        className="-m-2.5 p-2.5 text-gray-700 lg:hidden"
                        onClick={() => setSidebarOpen(true)}
                    >
                        <span className="sr-only">Ouvrir le menu</span>
                        <Bars3Icon
                            className=" bg-[#2765EC] text-white rounded-md h-6 w-6 m-3"
                            aria-hidden="true"
                        />
                    </button>
                </div>
            </div>
        </>
    );
}
