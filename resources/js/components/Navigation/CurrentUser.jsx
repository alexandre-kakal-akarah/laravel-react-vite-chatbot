import React, { useState, useEffect } from 'react';

const Session = () => {
    const [session, setSession] = useState(null);

    useEffect(() => {
        const fetchSession = async () => {
            const response = await fetch('http://localhost:8000/api/user');
            const data = response.json()
            setSession(data)
            console.log(data)
        };
    }, []);

    if (!session) {
        return <div>Chargement...</div>;
    }

    return (
        <div>
            <p>Session : {session}</p>
        </div>
    );
};

export default Session;
