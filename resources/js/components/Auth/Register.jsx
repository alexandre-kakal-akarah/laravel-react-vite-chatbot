import React from 'react'
import { useState } from 'react'
import styles from "./Register.module.scss";


const Register = () => {


    const [firstname, setFirstname] = useState("")
    const [username, setUsername] = useState("")
    const [email, setEmail] = useState("")
    const [gender, setGender] = useState("")
    const [town, setTown] = useState("")
    const [postal, setPostal] = useState("")
    const [street, setStreet] = useState("")
    const [password, setPassword] = useState("")
    const [password_confirmation, setPassword_confirmation] = useState("")


    const register = () => {


        if(password !== password_confirmation){
            alert("Les mots de passe ne correspondent pas")
            return
        }

        const requestOption = {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({
                name: firstname,
                username: username,
                email: email,
                gender: gender,
                town: town,
                postal: postal,
                street: street,
                password: password,
                role: "user"
            })}
            fetch("http://127.0.0.1:8000/api/register", requestOption)
            .then((response) => response.json())
            .then((data) => {
                console.log(data)
                localStorage.setItem("token", data.access_token)
            }
            )
        }


    return (
        <div>
            <form className={styles.form} action="">
                <input className={styles.fields} required type="text" name="firstname" id="firstname" placeholder="Prénom" onChange={(e)=>{setFirstname(e.target.value)}} />
                <input className={styles.fields} required type="text" name="username" id="username" placeholder="Pseudo" onChange={(e)=>{setUsername(e.target.value)}} />
                <input className={styles.fields} required type="text" name="email" id="email" placeholder="Email" onChange={(e)=>{setEmail(e.target.value)}} />
                <select className={styles.fields} required name="gender" id="gender" onChange={(e)=>{setGender(e.target.value)}}>
                    <option value="x">Genre</option>
                    <option value="man">Homme</option>
                    <option value="women">Femme</option>
                    <option value="x">Ne pas renseigner</option>
                </select>
                <input className={styles.fields} required  type="text" name="town" id="town" placeholder="Ville" onChange={(e)=>setTown(e.target.value)}/>
                <input className={styles.fields} required type='number' name="postal" id="postal" placeholder="Code postal" onChange={(e)=>setPostal(e.target.value)} />
                <input className={styles.fields} required type="text" name="street" id="street" placeholder="Rue" onChange={(e)=>setStreet(e.target.value)}/>
                <input className={styles.fields} required  type="password" name="password" id="password" placeholder="Mot de passe" onChange={(e)=>setPassword(e.target.value)}/>
                <input className={styles.fields} required type="password" name="password_confirmation" id="password_confirmation" placeholder="Confirmer le mot de passe" onChange={(e)=>setPassword_confirmation(e.target.value)}/>
                <input className={styles.button} type="submit" value="S'inscrire" onClick={(e)=>{
                    e.preventDefault()
                    register()
                }}/>
                <input className={styles.cancel} type="submit" value="Annuler" onClick={(e)=>{
                    e.preventDefault()
                    register()
                }}/>
            </form>
        </div>
    )
}

export default Register