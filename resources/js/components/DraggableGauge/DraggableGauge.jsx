import React, { useEffect, useRef, useState } from "react";
import style from "./DraggableGauge.module.scss";
import { gsap } from "gsap";
import { Draggable } from "gsap/Draggable";

gsap.registerPlugin(Draggable);

const DraggableGauge = () => {
    const [indicator, setIndicator] = useState(40);
    const gaugeRef = useRef(null);
    const indicatorRef = useRef(null);

    const onDragEvent = () => {
        console.log("dragging");
    };

    const onThrowComplete = () => {
        console.log("throw complete");
    };

    useEffect(() => {
        const draggable = Draggable.create(indicatorRef.current, {
            type: "x",
            bounds: gaugeRef.current,
            minDuration: 0.25,
            maxDuration: 1.5,
            overshootTolerance: 0,
            onThrowComplete: onThrowComplete,
            onDrag: onDragEvent,
        });
    }, []);

    return (
        <div className={style.GaugeWrapper}>
            <div ref={gaugeRef} className={style.GaugeWrapper__Gauge}></div>
            <div ref={indicatorRef} className={style.GaugeWrapper__Indicator}>
                {indicator}
            </div>
        </div>
    );
};

export default DraggableGauge;

// see https://greensock.com/docs/v3/Plugins/Draggable
// see https://codepen.io/motionimaging/pen/bpNmjJ?editors=1010
