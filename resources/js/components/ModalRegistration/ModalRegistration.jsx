import React from "react";
import styles from "./ModalRegistration.module.scss";
import { useRegistrationStore } from "../../store/registrationStore";

const ModalRegistration = () => {
    const { registrationIsOpen, setRegistrationIsOpen } =
        useRegistrationStore();

    const closeHandler = () => {
        setRegistrationIsOpen(false);
    };

    return (
        <>
            {registrationIsOpen && (
                <form>
                    <input
                        className={styles.Fields}
                        type="text"
                        name="name"
                        id="register-name"
                        placeholder="Nom"
                    />
                    <input
                        className={styles.Fields}
                        type="text"
                        name="username"
                        id="register-user-name"
                        placeholder="nom d'utilisateur"
                    />
                    <input
                        className={styles.Fields}
                        type="email"
                        name="email"
                        id="register-email"
                        placeholder="email"
                    />
                    <input
                        className={styles.Fields}
                        type="password"
                        name="password"
                        id="password"
                        placeholder="mot de passe"
                    />
                    <input
                        type="button"
                        value="Inscription"
                        className={styles.Registration}
                        onClick={(e) => {
                            e.preventDefault();
                            sendMessage();
                            onClickHandler();
                        }}
                    />
                    <input
                        type="button"
                        value="Annuler"
                        className={styles.Registration}
                        onClick={(e) => {
                            e.preventDefault();
                            sendMessage();
                            onClickHandler();
                        }}
                    />
                </form>
            )}
        </>
    );
};

export default ModalRegistration;
