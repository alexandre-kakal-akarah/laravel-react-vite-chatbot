import React from "react";
import styles from "./Tags.module.scss";
import { useCategoryStore } from "../../store/categoryStore";

const Tags = ({ tagName }) => {
    const { setCategoryText } = useCategoryStore();
    return (
        <div className={styles.TagContainer}>
            {tagName.map((tag, index) => {
                return (
                    <p
                        className={styles.TagContainer__Tag}
                        onClick={() => setCategoryText(tag)}
                        key={index}
                    >
                        {tag}
                    </p>
                );
            })}
        </div>
    );
};

export default Tags;
