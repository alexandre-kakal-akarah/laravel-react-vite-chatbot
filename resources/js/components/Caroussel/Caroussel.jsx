import React, { useState } from "react";
import styles from "./Caroussel.module.scss";
import Slider from "react-slick";
import Slide from "../Slide/Slide";
import Tags from "../Tag/Tags";
import ProductModal from "../ProductModal/ProductModal";

const Caroussel = ({ message }) => {
    const [productData, setProductData] = useState(null);
    if (message.message == null || message.message == "") return null;

    if (!message.data.products) {
        return <Tags tagName={message.data.data} />;
    }
    if (message.provider === "client") {
        return null;
    }

    const settings = {
        className: "center",
        centerMode: true,
        dots: true,
        infinite: true,
        centerPadding: "120px",
        slidesToShow: 1,
        speed: 500,
    };

    const products = [message.data.products];
    console.log(products[0]);

    return (
        <>
            <Slider {...settings}>
                {products[0].map((data) => {
                    return (
                        <Slide
                            image={data.image}
                            title={data.name}
                            // stars={data.rate}
                            price={data.price}
                            data={data}
                            setProductModal={setProductData}
                            key={data.name}
                        />
                    );
                })}
            </Slider>
            {productData && <ProductModal productData={productData} />}
        </>
    );
};
export default Caroussel;
