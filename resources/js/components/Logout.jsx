import React from "react";
import axios from "axios";
import { ArrowRightOnRectangleIcon } from "@heroicons/react/24/outline";

const LogoutButton = () => {
    const handleLogout = async () => {
        try {
            await axios.post("/logout");
            // rediriger l'utilisateur vers la page de connexion ou l'accueil
            window.location.href = "/login";
        } catch (error) {
            console.error(error);
        }
    };

    return (
        <li className="mt-auto cursor-pointer">
            <div className="group -mx-2 flex gap-x-3 rounded-md p-2 text-sm font-semibold leading-6 text-indigo-200 hover:bg-[#FFFFFF] hover:text-[#2765EC]">
                <ArrowRightOnRectangleIcon
                    className="h-6 w-6 shrink-0 text-indigo-200 group-hover:text-[#2765EC]"
                    aria-hidden="true"
                />
                <a
                    onClick={handleLogout}
                    className="h-6 w-6 shrink-0 text-indigo-200 group-hover:text-[#2765EC]"
                >
                    Deconnexion
                </a>
            </div>
        </li>
    );
};

export default LogoutButton;
