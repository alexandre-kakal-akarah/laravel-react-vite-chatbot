import React, {useEffect} from "react";
import {useState} from "react";

const Form = (props) => {

    const  [message, setMessage] = useState();
    let  responseId
    const [data, setData] = useState([])
    const [filteredData, setFilteredData] = useState([])
    const [searchText, setSearchText] = useState("")
    const [isVisible, setIsVisible] = useState(false)


    useEffect(() => {
        fetch(`http://localhost:8000/api/${props.route}`)
        .then(response => response.json())
        .then(responseData => setData(responseData))
    }, []);

    useEffect(()=>{
        setFilteredData(data)
    }, [data])

    function getResponseId(event){
        const value = event.target.value;
        responseId = (value);
        console.log(responseId)
    }

    function getMessage(event){
        if(!event) return;
        const value = event.target.value;
        setMessage(value);
    }
    function CreateKeyword(){
        console.log(message)
        console.log(responseId)

        try {
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    "word": message,
                    "response_id": responseId
                })
            };
            fetch(`http://localhost:8000/api/${props.route}`, requestOptions)
        } catch (e) {
            console.log('Erreur :' + e);
        }
    }

    return (
            <form className="h-[80vh] flex flex-col justify-items-center items-center border-2 border-[#0B6FED] rounded-2xl px-5 py-5 relative">
                <h2 className="text-center text-[#0F6AE6] font-[Oswald] py-2 px-2 text-4xl uppercase">{props.title}</h2>
                <input type="text" placeholder="Rechercher" className="border-1 border-[#0F6AE6] rounded-3xl w-[90%] px-6 my-5"
                onChange={(e) => {
                    setSearchText(e.target.value)
                    setFilter
                }}
                ></input>
                <ul className="flex justify-center flex-col overflow-scroll h-[80%]">
                    {filteredData.map((data, index)=>{
                        return(
                            <div className="flex items-center justify-between gap-5">
                                <li className="w-[90%] py-2" key={data.id + index}>{data[props.info]}</li>
                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="20" viewBox="0 0 36 27">
                                    <g id="Icon_feather-delete" data-name="Icon feather-delete" transform="translate(0 -4.5)">
                                        <path id="Tracé_3" data-name="Tracé 3" d="M31.5,6H12L1.5,18,12,30H31.5a3,3,0,0,0,3-3V9A3,3,0,0,0,31.5,6Z" fill="none" stroke="#0079fb" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                                        <path id="Tracé_4" data-name="Tracé 4" d="M27,13.5l-9,9" fill="none" stroke="#0079fb" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                                        <path id="Tracé_5" data-name="Tracé 5" d="M18,13.5l9,9" fill="none" stroke="#0079fb" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                                    </g>
                                </svg>
                            </div>
                    )})}
                </ul>

                <div className={isVisible == true ? "absolute bottom-20 flex flex-col items-center w-[50%] gap-5 bg-white py-3 border-2 border-[#0B6FED] rounded-2xl" : "hidden absolute bottom-20 flex flex-col items-center w-[50%] gap-5 bg-white py-3 border-2 border-[#0B6FED] rounded-2xl"}>
                    <input type="text" placeholder="Phrase" className="w-4/5" onChange={(e)=>setMessage(e.target.value)}/>
                    <select name="action" id="action" className="w-4/5">
                        <option value="Action">Action</option>
                        <option value="Action">Action</option>
                        <option value="Action">Action</option>
                        <option value="Action">Action</option>
                    </select>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="77" height="73" viewBox="0 0 77 73">
                    <defs>
                        <linearGradient id="linear-gradient" x1="0.389" y1="-6.264" x2="0.5" y2="1" gradientUnits="objectBoundingBox">
                        <stop offset="0" stop-color="#3942af"/>
                        <stop offset="1" stop-color="#0079fb"/>
                        </linearGradient>
                        <filter id="Rectangle_250" x="0" y="0" width="77" height="73" filterUnits="userSpaceOnUse">
                        <feOffset dx="1" dy="1" input="SourceAlpha"/>
                        <feGaussianBlur stdDeviation="5" result="blur"/>
                        <feFlood flood-opacity="0.49"/>
                        <feComposite operator="in" in2="blur"/>
                        <feComposite in="SourceGraphic"/>
                        </filter>
                    </defs>
                    <g id="Groupe_123" data-name="Groupe 123" transform="translate(-698 2938)">
                        <g transform="matrix(1, 0, 0, 1, 698, -2938)" filter="url(#Rectangle_250)">
                        <rect id="Rectangle_250-2" data-name="Rectangle 250" width="47" height="43" rx="21.5" transform="translate(14 14)" fill="url(#linear-gradient)"/>
                        </g>
                        <path id="Icon_awesome-plus" data-name="Icon awesome-plus" d="M17.922,9.832h-6.2v-6.2A1.379,1.379,0,0,0,10.34,2.25H8.961A1.379,1.379,0,0,0,7.582,3.629v6.2h-6.2A1.379,1.379,0,0,0,0,11.211V12.59a1.379,1.379,0,0,0,1.379,1.379h6.2v6.2a1.379,1.379,0,0,0,1.379,1.379H10.34a1.379,1.379,0,0,0,1.379-1.379v-6.2h6.2A1.379,1.379,0,0,0,19.3,12.59V11.211A1.379,1.379,0,0,0,17.922,9.832Z" transform="translate(725.85 -2914.386)" fill="#fff"/>
                    </g>
                    </svg>

                </div>

            <a className={isVisible == true ? "bg-red-600 text-white rounded-2xl py-1 px-3 w-full text-center" : "bg-[#0573F3] text-white rounded-2xl py-1 px-3 w-full text-center" }
        
            onClick={(e)=> {
                e.preventDefault
                isVisible == true ? setIsVisible(false) : setIsVisible(true)
            }}>{isVisible == true ? `Annuler l'ajout` : `Ajouter une phrase`}</a>
            </form>
    );
};

export default Form;
