import React, {useEffect} from "react";
import {useState} from "react";

const ProductForm = (props) => {

    const  [message, setMessage] = useState();
    let  responseId
    const [data, setData] = useState([])
    const [filteredData, setFilteredData] = useState([])
    const [searchText, setSearchText] = useState("")
    const [isVisible, setIsVisible] = useState(false)


    useEffect(() => {
        fetch("http://localhost:8000/api/product/")
        .then(response => response.json())
        .then(responseData => setData(responseData))
    }, []);

    useEffect(()=>{
        setFilteredData(data)
    }, [data])
    
    function setFilter() {
        setData(data.filter((filtre) => filtre.includes(searchText)));
    }

    function getResponseId(event){
        const value = event.target.value;
        responseId = (value);
        console.log(responseId)
    }

    function getKeywordsId(event){
        const value = event.target.value;
        keywordsId = (value);
        console.log(keywordsId)
    }

    function getMessage(event){
        if(!event) return;
        const value = event.target.value;
        setMessage(value);
    }

    function CreateKeyword(){
        console.log(message)
        console.log(responseId)

        try {
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    "word": message,
                    "response_id": responseId
                })
            };
            fetch('http://localhost:8000/api/keywords/', requestOptions)
        } catch (e) {
            console.log('Erreur :' + e);
        }
    }

    return (
        <div className="flex justify-center items-center pl-[3%] flex-col">
            <form className="h-[80vh] w-[27vw] flex flex-col justify-items-center items-center border-2 border-[#0B6FED] rounded-2xl px-5 py-5 relative">
                <h2 className="text-center text-[#0F6AE6] font-[Oswald] py-2 px-2 text-4xl">Produits</h2>
                <input type="text" placeholder="Rechercher" className="border-1 border-[#0F6AE6] rounded-3xl w-[90%] px-6 my-5"
                onChange={(e) => {
                    setSearchText(e.target.value)
                    setFilter
                }}
                on></input>

                <ul className="flex justify-center w-full flex-col overflow-scroll h-[80%]">
                    {filteredData.map((data, index)=>{
                        return(
                            <div className="flex items-center justify-between gap-5">
                                <li className="w-[90%] py-2" key={data.id + index}>{data.name}</li>
                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="20" viewBox="0 0 36 27">
                                    <g id="Icon_feather-delete" data-name="Icon feather-delete" transform="translate(0 -4.5)">
                                        <path id="Tracé_3" data-name="Tracé 3" d="M31.5,6H12L1.5,18,12,30H31.5a3,3,0,0,0,3-3V9A3,3,0,0,0,31.5,6Z" fill="none" stroke="#0079fb" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                                        <path id="Tracé_4" data-name="Tracé 4" d="M27,13.5l-9,9" fill="none" stroke="#0079fb" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                                        <path id="Tracé_5" data-name="Tracé 5" d="M18,13.5l9,9" fill="none" stroke="#0079fb" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
                                    </g>
                                </svg>
                            </div>
                    )})}
                </ul>
            </form>
        </div>
    );
};

export default ProductForm;
