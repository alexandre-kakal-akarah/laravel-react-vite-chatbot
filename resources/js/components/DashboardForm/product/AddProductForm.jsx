import React, {useEffect} from "react";
import {useState} from "react";

const AddProductForm = (props) => {

    const  [message, setMessage] = useState();
    let  responseId
    const [data, setData] = useState([])
    const [filteredData, setFilteredData] = useState([])
    const [searchText, setSearchText] = useState("")
    const [isVisible, setIsVisible] = useState(false)


    useEffect(() => {
        fetch("http://localhost:8000/api/categories/")
        .then(response => response.json())
        .then(responseData => setData(responseData))
    }, []);

    useEffect(()=>{
        setFilteredData(data)
    }, [data])
    
    
    function setFilter() {
        setData(data.filter((filtre) => filtre.includes(searchText)));
    }

    function getResponseId(event){
        const value = event.target.value;
        responseId = (value);
        console.log(responseId)
    }

    function getMessage(event){
        if(!event) return;
        const value = event.target.value;
        setMessage(value);
    }
    function CreateKeyword(){
        console.log(message)
        console.log(responseId)

        try {
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    "word": message,
                    "response_id": responseId
                })
            };
            fetch('http://localhost:8000/api/keyword/', requestOptions)
        } catch (e) {
            console.log('Erreur :' + e);
        }
    }

    return (
            <form className="h-[80vh] flex flex-col justify-items-center items-center border-2 border-[#0B6FED] rounded-2xl px-5 py-3 relative">
                <input type="text" placeholder="Nom du nouveau produit" className="border-1 border-[#0F6AE6] rounded-3xl w-[50%] px-6 my-3"></input>
                <input type="file"></input>
                <input type="text" placeholder="Description du produit" className="border-1 border-[#0F6AE6] rounded-3xl w-[90%] px-6 my-3"></input>
                <div className="flex items-center justify-between gap-12 w-[90%]">
                    <select className="border-1 border-[#0F6AE6] rounded-3xl w-[90%] px-6 my-3" placeholder="Couleur">
                        <option>Choisir la taille </option>
                        <option>Enfant</option>
                        <option>Femme</option>
                        <option>Enfant</option>
                    </select>                    
                    <select className="border-1 border-[#0F6AE6] rounded-3xl w-[90%] px-6 my-3" placeholder="Couleur">
                        <option>Choisir la catégorie </option>
                        <option>Enfant</option>
                        <option>Femme</option>
                        <option>Enfant</option>
                    </select>
                </div>
                <div className="flex items-center justify-between gap-12 w-[90%]">
                    <select className="border-1 border-[#0F6AE6] rounded-3xl w-[90%] px-6 my-3" placeholder="Couleur">
                        <option>Choisir le genre</option>
                        <option>Enfant</option>
                        <option>Femme</option>
                        <option>Enfant</option>
                    </select>                    
                    <select className="border-1 border-[#0F6AE6] rounded-3xl w-[90%] px-6 my-3" placeholder="Couleur">
                        <option>Choisir le stock </option>
                        <option>1</option>
                        <option>2</option>
                        <option>+3</option>
                    </select>
                </div>
                <div className="flex items-center justify-between gap-12 w-[90%]">
                    <input type="text" placeholder="Prix" className="border-1 border-[#0F6AE6] rounded-3xl w-[90%] px-6 my-3"></input>
                    <select className="border-1 border-[#0F6AE6] rounded-3xl w-[90%] px-6 my-3" placeholder="Couleur">
                        <option>Choisir la couleur</option>
                        <option>Rouge</option>
                        <option>Jaune</option>
                        <option>Bleu</option>
                        <option>Noir</option>
                        <option>Blanc</option>
                        <option>Vert</option>
                    </select>
                </div>
                <button className="bg-[#0573F3] text-white rounded-2xl py-1 px-3 w-[50%] text-center">Ajouter le nouveau produit</button>
            </form> 
    );
};

export default AddProductForm;
