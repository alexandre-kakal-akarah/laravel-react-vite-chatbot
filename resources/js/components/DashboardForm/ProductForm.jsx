import React from "react";

export default function ProductForm(){
    return(
        <>
            <h1 className="text-center text-5xl py-5 text-[#1762DB] font-['Oswald']">
                ChatBot Sneak Me
            </h1>
            <h2 className="text-center font-['Nunito Sans'] text-[#1762DB]">
                Bonjour {name}, Bienvenue sur le tableau de bord de votre chatbot.
            </h2>
            <form className="inline-block border-solid border-2 border-red-700">
                <input placeholder="Entrez le mot clé"></input>
                <button type="submit">Ajouter</button>
            </form>
            <br></br>
            <div className="grid grid-flow-col-3 border-solid border-2 border-[#0B6FED] text-center">
                <div>
                    <input type="text" placeholder="Nom du produit"
                           className="space-x-2 px-4 py-3 rounded-full border-2 border-[#0079fb]"></input>
                </div>
                <div>
                    <input type="text" placeholder="Description"
                           className="space-x-2 px-4 py-3 rounded-full border-2 border-[#0079fb]"></input>
                </div>
                <div>
                    <input type="text" placeholder="Cliquez pour ajouter une image"
                           className="space-x-2 px-4 py-3 rounded-full border-2 border-[#0079fb]"></input>
                </div>

                {/* Menu Déroulant Taille */}
                <div>
                    <select
                        className="form-multiselect space-x-2 px-4 py-3 pr-5 rounded-full border-2 border-[#0079fb]">
                        <option>Choisir la taille</option>
                        <option>33</option>
                        <option>34</option>
                        <option>35</option>
                        <option>35.5</option>
                        <option>36</option>
                        <option>37</option>
                        <option>37.5</option>
                        <option>38</option>
                        <option>38.5</option>
                        <option>39</option>
                        <option>40</option>
                        <option>41</option>
                        <option>42</option>
                        <option>43</option>
                        <option>44</option>
                        <option>45</option>
                        <option>46</option>
                        <option>47</option>
                    </select>
                </div>

                <div>
                    {/* Menu Déroulant Genre */}
                    <select
                        className="form-multiselect flex space-x-2 px-4 py-3 rounded-full border-2 border-[#0079fb]">
                        <option>Choisir le genre</option>
                        <option>Homme</option>
                        <option>Femme</option>
                        <option>Enfant</option>
                    </select>
                </div>

                {/* Menu Déroulant Catégories */}
                <select className="form-multiselect flex space-x-2 px-4 py-3 rounded-full border-2 border-[#0079fb]">
                    <option>Choisir la catégorie</option>
                    <option>Burger 1</option>
                    <option>Burger 2</option>
                    <option>Burger 3</option>
                </select>

                {/* Menu Déroulant Stock */}
                <select className="form-multiselect flex space-x-2 px-4 py-3 rounded-full border-2 border-[#0079fb]">
                    <option>Choisir le stock</option>
                    <option>En Rupture</option>
                    <option>En Stock</option>
                </select>

                {/* Menu Déroulant Couleur */}
                <select className="form-multiselect flex space-x-2 px-4 py-3 rounded-full border-2 border-[#0079fb]">
                    <option>Choisir la couleur</option>
                    <option>Rouge</option>
                    <option>Bleu</option>
                    <option>Rose</option>
                    <option>Jaune</option>
                    <option>Vert</option>
                    <option>Noir</option>
                    <option>Blanc</option>
                    <option>Orange</option>
                    <option>Marron</option>
                </select>
                <input type="number" placeholder="Définir un prix"
                       className="flex space-x-2 px-4 py-3 rounded-full border-2 border-[#0079fb]"></input>
                <button type="submit" className="bg-[#0B6FED] text-white px-3 py-1 rounded-full flex space-x-2">Ajouter
                    le produit
                </button>
            </div>
        </>
    )
}
