import React, { useState } from "react";
import styles from "./Slide.module.scss";

const Slide = ({ image, title, price, setProductModal, data, key }) => {
    const [isDragging, setIsDragging] = useState(false);
    const [isClicking, setIsClicking] = useState(false);
    const [isMouseDown, setIsMouseDown] = useState(false);

    //if (stars.stars)

    /**
     * @param {number} stars On passe en paramètre le nombre d'étoiles à afficher
     * @description Cette fonction permet d'afficher les étoiles en fonction du nombre d'étoiles à afficher
     * @returns {JSX.Element} On retourne le JSX à afficher
     */
    const renderStars = (stars) => {
        // ton code ici
        // tu peux utiliser un for pour afficher les étoiles pleines ou vides
        // tu boucles sur le nombre d'étoiles à afficher
        // let starsArray = [];
        // for (let i = 0; i < 5; i++) {
        //     // tu affiches les étoiles pleines
        //     if (i < stars) {
        //         starsArray.push(
        //             <img src="/assets/etoile-p.svg" alt="etoiles" />
        //         );
        //     } else {
        //         starsArray.push(
        //             <img src="/assets/etoile-v.svg" alt="etoiles" />
        //         );
        //     }
        // }
        // return starsArray;
    };

    const handleMouseDown = () => {
        setIsMouseDown(true);
        setIsDragging(false);
    };

    const handleMouseMove = () => {
        if (isMouseDown) {
            setIsDragging(true);
        } else {
            setIsDragging(false);
        }
    };

    const handleMouseUp = () => {
        if (isDragging) {
            // C'est un drag
            setIsDragging(false);
        } else {
            // C'est un clic
            setIsClicking(true);
        }
    };

    const handleClick = () => {
        if (isClicking) {
            console.log(data, "data");
            setProductModal(data);
        }
        setIsClicking(false);
    };

    return (
        <div
            className={styles.Caroussel}
            onMouseDown={handleMouseDown}
            onMouseMove={handleMouseMove}
            onMouseUp={handleMouseUp}
            onClick={handleClick}
            key={key}
        >
            {/* <p>{rate} avis</p>
            <div className={styles.etoile}>{renderStars(stars)}</div> */}

            <img src={image} alt="chaussure" className={styles.chaussure} />
            <p className={styles.sneaker}>{title}</p>
            <p className={styles.price}>{price} €</p>
        </div>
    );
};

export default Slide;
