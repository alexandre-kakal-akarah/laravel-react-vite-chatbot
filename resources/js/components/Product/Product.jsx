import React from "react";
import styles from "./Product.module.scss";
import Slide from "../Slide/Slide";


const Product = ({ url, image, title, stars, rate, price, taille, color, desc }) => {

    if (message.message == null || message.message == "") return null;
    if (message.provider === "client") {
        return null;
    }

    const tempData = [
        {
            id: 1,
            title: "Jordan IV",
            url: "dsfds",
            image: "/assets/jordan4.png",
            stars: 4,
            rate: "dzfsd",
            price: "400",
            taille: "42",
            desc: "gfdgqdfgqdfSFDSGFDSGFfdfdg",
        }
    ];

    return (
        <div className={styles.Caroussel}>
            <p>{rate}</p>
            <div className={styles.etoile}>
                {renderStars(stars)}
            </div>
    
    
            
            <img src={image} alt="chaussure" className={styles.chaussure}/>
            <p className={styles.sneaker}>{title}</p>
            <p className={styles.price}>{price}</p>
            <p className={styles.taille}>{taille}</p>
            <p className={styles.desc}>{desc}</p>
        </div>
        )
}

export default Product;