import React, { useRef, useEffect } from "react";
import styles from "./ProductModal.module.scss";
import Message from "../Message/Message";

const ProductModal = ({ productData }) => {
    console.log(productData, "productData");
    if (!productData) return null;
    console.log(productData, "productData");
    const { color, description, image, name, price, size } = productData;
    const RenderSizes = (sizes) => {
        if (!sizes) return null;
        sizes.map((size) => {
            return (
                <button key={size} className={styles.ProductModal__Size}>
                    {size}
                </button>
            );
        });
    };

    const serverMessage = {
        provider: "server",
        message: `Voici le produit ${name}`,
    };

    const scrollToTop = useRef();

    useEffect(() => {
        scrollToTop.current.scrollIntoView({ behavior: "smooth" });
    }, [productData]);

    return (
        <>
            <div ref={scrollToTop} />
            <Message message={serverMessage} />
            <div className={styles.ProductModal}>
                <h2 className={styles.ProductModal__Title}>{name}</h2>
                <img
                    src={image}
                    alt="product"
                    className={styles.ProductModal__Image}
                />
                <h3 className={styles.ProductModal__SubTitle}>Tailles</h3>
                {productData && RenderSizes(productData.sizes)}
                <h3 className={styles.ProductModal__SubTitle}>Couleur</h3>
                {/* style text-align center in p */}
                <p className={styles.ProductModal__Color}>{color}</p>
                {/* TO DO: add color picker */}
                <h3 className={styles.ProductModal__SubTitle}>Description</h3>
                <p>{description}</p>
            </div>
        </>
    );
};

export default ProductModal;
