import React from "react";
import styles from "./Form.module.scss";
import { useCategoryStore } from "../../store/categoryStore";

const Form = ({ setState, message }) => {
    const { categoryText, setCategoryText } = useCategoryStore();
    const onClickHandler = () => {
        //retrieveMessages();
    };

    const sendMessage = () => {
        const token = localStorage.getItem("token");
        console.log(token, "token");
        const inputValue = document.querySelector("input[name=message]").value;
        const requestOptions = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
            },

            body: JSON.stringify({ message: inputValue }),
        };
        fetch(`http://localhost:8000/api/messages`, requestOptions)
            .then((response) => response.json())
            .then((data) => {
                console.log(data, "data");
                if (inputValue === "") return;
                const newMessage = {
                    provider: "server",
                    message: data.response,
                    action: data.action,
                    data: data,
                };
                const input = document.querySelector("input[name=message]");
                setState([
                    ...message,
                    { provider: "client", message: input.value },
                    newMessage,
                ]);
                input.value = "";
            });
        setCategoryText(null);
    };
    return (
        <div>
            <form id="send-message" className={styles.sendMessage}>
                <input
                    type="text"
                    name="message"
                    id=""
                    className={styles.Form}
                    placeholder="Entrez votre message ..."
                    value={categoryText ? "eos " + categoryText : null}
                />
                <input
                    type="image"
                    id="image"
                    alt="submit"
                    src="../../../assets/submit.png"
                    className={styles.Submit}
                    onClick={(e) => {
                        e.preventDefault();
                        sendMessage();
                        onClickHandler();
                    }}
                ></input>
            </form>
        </div>
    );
};

export default Form;
