import React from "react";
import styles from "./Header.module.scss";

const Header = ({ isOpen, setIsOpen }) => {
    const openHandler = () => {
        setIsOpen(true);
    };
    const closeHandler = () => {
        setIsOpen(false);
    };

    return (
        <div className={`${styles.Header} place-content-center flex text-3xl`}>
            <img
                src="../../../assets/personnage.png"
                alt="personnage"
                className={styles.Perso}
            />
            <p className="absolute top-10">CHATBOT</p>
            {isOpen ? (
                <img
                    src="../../../assets/Croix.png"
                    alt="croix"
                    id="croix"
                    className={styles.croix}
                    onClick={closeHandler}
                />
            ) : (
                <img
                    src="../../../assets/open.png"
                    alt="croix"
                    id="croix"
                    className={styles.open}
                    onClick={openHandler}
                />
            )}
        </div>
    );
};

export default Header;
