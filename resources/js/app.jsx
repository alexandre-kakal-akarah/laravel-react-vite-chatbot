import "../css/app.css";
import "../style/globals.scss";

import ReactDOM from "react-dom/client";
import React from "react";
import { AppWrapper } from "./containers/AppWrapper";

ReactDOM.createRoot(document.getElementById("app")).render(<AppWrapper />);
