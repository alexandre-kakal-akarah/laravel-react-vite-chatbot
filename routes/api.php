<?php

use App\Http\Controllers\api\KeywordController;
use App\Http\Controllers\api\OrderController;
use App\Http\Controllers\api\OrderItemController;
use App\Http\Controllers\api\ProductController;
use App\Http\Controllers\api\ResponseController;
use App\Http\Controllers\api\CategoryController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\MessageController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

//Route::get('/messages', [MessageController::class, 'index']);
Route::post('/messages', MessageController::class);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::apiResource('messages', 'api\MessageController');



Route::post('/register', 'AuthController@register');
Route::post('/login', 'AuthController@login');
Route::post('/messages', MessageController::class);


Route::middleware(['optionalAuth:sanctum'])->group(function () {
    Route::post('/messages', MessageController::class);
});

Route::middleware('auth:sanctum')->group(function () {
    Route::apiResource('/keyword', KeywordController::class);
    Route::apiResource('/response', ResponseController::class);
    Route::apiResource('/product', ProductController::class);
    Route::apiResource('/category', CategoryController::class);
    Route::apiResource('/order', OrderController::class);
    Route::apiResource('/orderitem', OrderItemController::class);
    Route::get('/me', 'AuthController@me');
});