<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Category;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Response;
use App\Models\Keyword;
use App\Models\Action;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $actions = [
            'registration' => 5,
            'connexion' => 5,
            'commander' => 4,
            'ajout-panier' => 4,
            'consulter-panier' => 3,
            'categorie' => 2,
            'catalogue' => 1,
        ];

        foreach ($actions as $action => $weight) {
            Action::create([
                'action' => $action,
                'weight' => $weight,
            ]);
        }
        User::factory(10)->create();
        Category::factory(10)->create();
        Product::factory(10)->create();
        Order::factory(25)->create();
        OrderItem::factory(56)->create();
        Response::factory(25)->create();
        Keyword::factory(50)->create();


        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
  
    }
}
