<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use \App\Models\User;
use \App\Models\Product;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\OrderItem>
 */
class OrderItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'order_id'=>User::all()->random()->id,
            'product_id'=>Product::all()->random()->id,
            'quantity'=>fake()->numberBetween(1, 5),
            'price'=> fake()-> numberBetween(20, 100),
        ];
    }
}
