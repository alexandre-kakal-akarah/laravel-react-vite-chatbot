<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use \App\Models\Category;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->word(),
            'description' => fake()->sentences(6, true),
            'category_id' => Category::all()->random()->id,
            'price' => fake()->numberBetween(50, 10000),
            'image' => fake()->imageUrl(),
            'size'=>fake()->numberBetween(34, 45),
            'color'=>fake()->colorName(),
            'rate'=>fake()->numberBetween(0, 100),
        ];
    }
}
